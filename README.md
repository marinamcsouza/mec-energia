# MEC-Energia

Este repositório contém a implementação da API do sistema MEC-Energia. O sistema MEC-Energia é um projeto de código aberto que ajuda instituições de ensino superior (IES) a gerenciar e avaliar seus contratos de energia elétrica.

A documentação online do sistema está disponível em [Documentação](https://lappis-unb.gitlab.io/projects/mec-energia/documentacao)

## Descrição

A API do MEC-Energia fornece uma interface para acessar os dados do sistema. Ela permite que os usuários consultem informações sobre contratos, faturas, consumo e demanda. A API também permite que os usuários gerem relatórios de recomendações de ajustes nos contratos.

## Endpoints

A API do MEC-Energia oferece os seguintes endpoints:

| Endpoint | Descrição |
|---|---|
| `/contracts` | Lista todos os contratos. |
| `/contracts/[id]` | Retorna as informações de um contrato específico. |
| `/invoices` | Lista todas as faturas. |
| `/invoices/[id]` | Retorna as informações de uma fatura específica. |
| `/consumption` | Lista o consumo de energia de todas as IES. |
| `/consumption/[id]` | Retorna o consumo de energia de uma IES específica. |
| `/demand` | Lista a demanda contratada de todas as IES. |
| `/demand/[id]` | Retorna a demanda contratada de uma IES específica. |
| `/reports` | Lista todos os relatórios. |
| `/reports/[id]` | Retorna as informações de um relatório específico. |

## Como executar

Para acessar o banco, é recomendado que você tenha [Docker Compose](https://docs.docker.com/compose/install/) instalado em sua máquina.

Copie o arquivo `.env.dev` para `.env`:

```
cp .env.dev .env 
```

Levante os containers da API e do banco de dados com:

```sh
docker-compose up -d
```

Se tudo deu certo, a API do Django REST deve estar acessível em 
http://localhost:8000.

Além disso, a API também tem seus endpoints documentados no Swagger em
http://localhost:8000/api/swagger/schema/.

Para ter acesso completo dos dados nos dois links, você precisa criar um 
usuário. Leia como fazer isso em [seed](docs/seed.md#usuário).

Para derrubar os containers utilize:

```sh
docker-compose down
```

## Código de Conduta e Políticas

* Leia o [Código de Conduta](/CODE_OF_CONDUCT.md) do projeto;
* Veja as [Políticas](docs/politicas/branches-commits.md) do projeto.


## Documentação Extra de Configuração

Para saber mais sobre configuração de ambiente de desenvolvimento e 
outras coisas, acesse os seguintes links:

- **Comece aqui:** [ambiente de desenvolvimento](docs/ambiente-desenvolvimento.md)
- [Seed/popular](docs/seed.md)
- [Testes](docs/testes.md)
- [Depuração](docs/depuracao.md)
